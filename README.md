# Impulse Mind

[![pipeline status](https://gitlab.com/impulse_bot/impulse_mind/badges/master/pipeline.svg)](https://gitlab.com/impulse_bot/impulse_mind/commits/master) [![coverage report](https://gitlab.com/impulse_bot/impulse_mind/badges/master/coverage.svg)](https://gitlab.com/impulse_bot/impulse_mind/commits/master)

## Overview

Impulse is Todo Assistant LINE Bot made to help our users to control and manage their schedule.

Impulse Mind is one out of two part of our Todo Assistant LINE Bot. The Mind acts as a handler and actuator - receiving triggers from our LINE messaging platform, processing it, and returning response accordingly. This service is the bridge between our users from LINE platform to their saved todos. 

*Just like a being, the mind controls the acts*

## Application Access

The Impulse LINE bot can be added through this link:  

<a href="https://line.me/R/ti/p/%40nit4592h"><img height="36" border="0" alt="Tambah Teman" src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png"></a>

## Authors (A8)

* **Jonathan Christopher Jakub** - [Jonathanjojo](https://gitlab.com/Jonathanjojo)
* **Aryo Tinulardhi** - [Aryodh](https://gitlab.com/aryodh)
* **Nabila Febri Viola** - [nabilafv](https://gitlab.com/nabilafv)
* **Aziz FIkri Hudaya** - [Azizhudaya](https://gitlab.com/Azizhudaya)
* **Nadhifah Hanan** - [nadhifahhanan](https://gitlab.com/nadhifahhanan)

## Acknowledgements

* Advance Programming CS UI 2019