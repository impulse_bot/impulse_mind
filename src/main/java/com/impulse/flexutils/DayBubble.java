package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;

public class DayBubble {
    public Bubble create(Date day, PriorityQueue<ToDo> tasks) {
        final Box headerBlock = createHeaderBlock(day);
        final Box bodyBlock = createBodyBlock(tasks);
        final Box footerBlock = createFooterBlock();
        return Bubble.builder()
            .header(headerBlock)
            .body(bodyBlock)
            .footer(footerBlock)
            .build();
    }

    private Box createHeaderBlock(Date day) {
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(
                Text.builder()
                    .text(new SimpleDateFormat("EEE, dd/MM/yyyy").format(day))
                    .size(FlexFontSize.XL)
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .weight(Text.TextWeight.BOLD)
                    .color("#283592")
                    .build(),
                Separator.builder()
                    .color("#000000")
                    .build()
            ))
            .build();
    }

    private Box createBodyBlock(PriorityQueue<ToDo> tasks) {
        List<FlexComponent> bodyContents = new ArrayList<>();
        tasks.stream().filter(task -> !task.getCompletion()).forEach(task -> {
            if (task.getImportance()) {
                bodyContents.add(makeImportantButton(task));
            } else {
                bodyContents.add(makeDefaultButton(task));
            }
        });
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.MD)
            .contents(bodyContents)
            .build();
    }

    private Button makeImportantButton(ToDo task) {
        return Button.builder()
            .action(new PostbackAction(task.getName(),
                "action=DETAILS&data=" + (new PostbackData()).getData(task)))
            .flex(0)
            .color("#E01B84")
            .style(Button.ButtonStyle.PRIMARY)
            .gravity(FlexGravity.CENTER)
            .build();
    }

    private Button makeDefaultButton(ToDo task) {
        return Button.builder()
            .action(new PostbackAction(task.getName(),
                "action=DETAILS&data=" + (new PostbackData()).getData(task)))
            .flex(0)
            .color("#000000")
            .style(Button.ButtonStyle.PRIMARY)
            .gravity(FlexGravity.CENTER)
            .build();
    }

    private Box createFooterBlock() {
        return Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .contents(asList(
                Image.builder()
                    .url("https://drive.google.com/uc?export=view&id=17Vh1hZS78Zch335D7p4sdYgoDHe9BqXs")
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .size(Image.ImageSize.XS)
                    .aspectRatio(Image.ImageAspectRatio.R1TO1)
                    .aspectMode(Image.ImageAspectMode.Fit)
                    .build()
            ))
            .build();
    }
}