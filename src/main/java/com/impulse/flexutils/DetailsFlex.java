package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.function.Supplier;

import org.json.JSONObject;


public class DetailsFlex implements Supplier<FlexMessage> {
    private Bubble bubble;


    @Override
    public FlexMessage get() {
        return new FlexMessage("Details", this.bubble);
    }

    public FlexMessage get(ToDo task) {
        final Box bodyBlock = createBodyBlock(task);
        final Box footerBlock = createFooterBlock(task);
        this.bubble =
            Bubble.builder()
                .body(bodyBlock)
                .footer(footerBlock)
                .build();
        return get();
    }

    private Box createBodyBlock(ToDo task) {
        final Text name =
            Text.builder()
                .text(task.getName())
                .size(FlexFontSize.XL)
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.BOLD)
                .color("#6D64E8")
                .wrap(true)
                .build();

        final Text date =
            Text.builder()
                .text("Date")
                .margin(FlexMarginSize.LG)
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.REGULAR)
                .color("#E01B84")
                .build();

        final Text dateData =
            Text.builder()
                .text(task.getPeriod())
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.BOLD)
                .color("#000000")
                .build();

        final Text time =
            Text.builder()
                .text("Time")
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.REGULAR)
                .color("#E01B84")
                .build();

        final Text timeData =
            Text.builder()
                .text(task.getStartTime() + " - " + task.getEndTime())
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.BOLD)
                .color("#000000")
                .build();

        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .flex(0)
            .spacing(FlexMarginSize.NONE)
            .margin(FlexMarginSize.NONE)
            .contents(asList(
                name,
                date, dateData,
                time, timeData
            ))
            .build();
    }

    private Box createFooterBlock(ToDo task) {
        final JSONObject data = new PostbackData().getData(task);
        final Button delete =
            Button.builder()
                .action(new PostbackAction("DELETE", "action=DELETE&data=" + data))
                .color("#6D64E8")
                .style(Button.ButtonStyle.PRIMARY)
                .gravity(FlexGravity.CENTER)
                .build();

        final Button importance =
            task.getImportance()
                ?
                Button.builder()
                    .action(new PostbackAction("UNIMPORTANT", "action=UNIMPORTANT&data=" + data))
                    .color("#E01B84")
                    .style(Button.ButtonStyle.PRIMARY)
                    .gravity(FlexGravity.CENTER)
                    .build()
                :
                Button.builder()
                    .action(new PostbackAction("IMPORTANT", "action=IMPORTANT&data=" + data))
                    .color("#E01B84")
                    .style(Button.ButtonStyle.PRIMARY)
                    .gravity(FlexGravity.CENTER)
                    .build();

        final Button complete =
            task.getCompletion()
                ?
                Button.builder()
                    .action(new PostbackAction("INCOMPLETE", "action=INCOMPLETE&data=" + data))
                    .color("#6D64E8")
                    .style(Button.ButtonStyle.PRIMARY)
                    .gravity(FlexGravity.CENTER)
                    .build()
                :
                Button.builder()
                    .action(new PostbackAction("COMPLETE", "action=COMPLETE&data=" + data))
                    .color("#6D64E8")
                    .style(Button.ButtonStyle.PRIMARY)
                    .gravity(FlexGravity.CENTER)
                    .build();

        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.SM)
            .contents(asList(delete, importance, complete))
            .build();
    }
}