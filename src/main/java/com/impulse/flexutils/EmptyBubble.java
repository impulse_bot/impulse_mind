package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;


public class EmptyBubble {
    public Bubble create(String textHeader, String textBody) {
        final Box headerBlock = createHeaderBlock(textHeader);
        final Box bodyBlock = createBodyBlock(textBody);
        final Box footerBlock = createFooterBlock();
        return Bubble.builder()
            .header(headerBlock)
            .body(bodyBlock)
            .footer(footerBlock)
            .build();
    }

    private Box createHeaderBlock(String textHeader) {
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(
                Text.builder()
                    .text(textHeader)
                    .size(FlexFontSize.XL)
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .weight(Text.TextWeight.BOLD)
                    .color("#283592")
                    .wrap(true)
                    .build()
            ))
            .build();
    }

    private Box createBodyBlock(String textBody) {
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(
                Text.builder()
                    .text(textBody)
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .weight(Text.TextWeight.BOLD)
                    .color("#E01B84")
                    .wrap(true)
                    .build()
            ))
            .build();
    }

    private Box createFooterBlock() {
        return Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .contents(asList(
                Image.builder()
                    .url("https://drive.google.com/uc?export=view&id=17Vh1hZS78Zch335D7p4sdYgoDHe9BqXs")
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .size(Image.ImageSize.XS)
                    .aspectRatio(Image.ImageAspectRatio.R1TO1)
                    .aspectMode(Image.ImageAspectMode.Fit)
                    .build()
            ))
            .build();
    }
}