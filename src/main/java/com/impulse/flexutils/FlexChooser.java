package com.impulse.flexutils;

import com.linecorp.bot.model.message.FlexMessage;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.SortedMap;

import org.json.JSONObject;


public class FlexChooser {

    public static FlexMessage choose(String type) {
        switch (type.toUpperCase()) {
            case "WELCOME":
                return getWelcome();
            case "HELP":
                return getHelp();
            case "REMINDER":
                return getReminder();
            default:
                return null;
        }
    }

    public static FlexMessage choose(String type, JSONObject data)
        throws ParseException, UnsupportedEncodingException {
        ToDo task;
        SortedMap<Date, PriorityQueue<ToDo>> schedule;
        switch (type.toUpperCase()) {
            case "LIST":
                schedule = JsonConverter.convert(data);
                return getList(schedule);
            case "DETAILS":
                task = JsonConverter.makeTask(data);
                return getDetails(task);
            case "SHARE":
                schedule = JsonConverter.convert(data);
                return getShare(schedule);
            case "REVIEW":
                schedule = JsonConverter.convert(data);
                return getReview(schedule);
            default:
                return null;
        }
    }

    private static FlexMessage getWelcome() { 
        return new WelcomeFlex().get(); 
    }

    private static FlexMessage getList(SortedMap<Date,PriorityQueue<ToDo>> schedule) {
        return new ListCarousel().get(schedule);
    }

    private static FlexMessage getShare(SortedMap<Date, PriorityQueue<ToDo>> schedule)
        throws UnsupportedEncodingException {
        return new ShareFlex().get(schedule);
    }

    private static FlexMessage getHelp() { 
        return new HelpFlex().get(); 
    }

    private static FlexMessage getReview(SortedMap<Date, PriorityQueue<ToDo>> schedule) {
        return new ReviewFlex().get(schedule);
    }

    private static FlexMessage getReminder() { 
        return new ReminderFlex().get(); 
    }

    private static FlexMessage getDetails(ToDo task) { 
        return new DetailsFlex().get(task); 
    }
}