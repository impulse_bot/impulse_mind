package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;


public class HelpFlex implements Supplier<FlexMessage> {
    @Override
    public FlexMessage get() {
        final Box bodyBlock = createBodyBlock();
        final Box footerBlock = createFooterBlock();
        final Bubble bubble =
            Bubble.builder()
                .body(bodyBlock)
                .footer(footerBlock)
                .build();
        return new FlexMessage("Help", bubble);
    }

    private Box createBodyBlock() {
        List<FlexComponent> bodyContents = new ArrayList<>();

        bodyContents.add(
            Text.builder()
                .text("A Guide to Impulse")
                .size(FlexFontSize.XL)
                .align(FlexAlign.CENTER)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.BOLD)
                .color("#283592")
                .build());

        bodyContents.add(
            Separator.builder()
                .color("#000000")
                .build());

        bodyContents
            .add(createCommandText(
                "Add\nTask Name\nyyyy-MM-dd\nHH:mm (Start Time)\nHH:mm (End Time)"));
        bodyContents
            .add(createInfoText("Add the task to your ToDo list"));

        bodyContents
            .add(createCommandText("List"));
        bodyContents
            .add(createInfoText(
                "View your schedule and click one of your ToDo to see the details"));

        bodyContents
            .add(createCommandText("Review"));
        bodyContents
            .add(createInfoText("View your completed tasks"));

        bodyContents
            .add(createCommandText("Share"));
        bodyContents
            .add(createInfoText("View and share your schedule as String representation"));

        bodyContents
            .add(createCommandText("Help"));
        bodyContents
            .add(createInfoText("View list of commands of Impulse"));

        bodyContents.add(
            Separator.builder()
                .color("#000000")
                .build());

        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.SM)
            .contents(bodyContents)
            .build();
    }

    private Text createCommandText(String name) {
        return Text.builder()
            .text(name)
            .margin(FlexMarginSize.LG)
            .size(FlexFontSize.Md)
            .align(FlexAlign.START)
            .gravity(FlexGravity.CENTER)
            .weight(TextWeight.BOLD)
            .color("#E01B84")
            .wrap(true)
            .build();
    }

    private Text createInfoText(String name) {
        return Text.builder()
            .text(name)
            .size(FlexFontSize.SM)
            .align(FlexAlign.START)
            .gravity(FlexGravity.CENTER)
            .weight(TextWeight.REGULAR)
            .color("#000000")
            .wrap(true)
            .build();
    }

    private Box createFooterBlock() {
        return Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .contents(asList(
                Image.builder()
                    .url("https://drive.google.com/uc?export=view&id=17Vh1hZS78Zch335D7p4sdYgoDHe9BqXs")
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .size(Image.ImageSize.XS)
                    .aspectRatio(Image.ImageAspectRatio.R1TO1)
                    .aspectMode(Image.ImageAspectMode.Fit)
                    .build()
            ))
            .build();
    }
}