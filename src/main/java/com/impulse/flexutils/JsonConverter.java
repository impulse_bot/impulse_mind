package com.impulse.flexutils;

import java.text.ParseException;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonConverter {

    public static SortedMap<Date, PriorityQueue<ToDo>> convert(JSONObject data)
        throws ParseException {
        JSONArray arrData = data.getJSONArray("data");

        String id;
        String userId;
        String name;
        String date;
        String startTime;
        String endTime;
        boolean completion;
        boolean importance;

        SortedMap<Date, PriorityQueue<ToDo>> resultMap =
            new TreeMap<Date, PriorityQueue<ToDo>>(
                (task1date, task2date) -> task1date.compareTo(task2date));

        for (int i = 0; i < arrData.length(); i++) {
            id = arrData.getJSONObject(i).getString("id");
            userId = arrData.getJSONObject(i).getString("userId");
            name = arrData.getJSONObject(i).getString("todoName");
            date = arrData.getJSONObject(i).getString("date");
            startTime = arrData.getJSONObject(i).getString("timeStart");
            endTime = arrData.getJSONObject(i).getString("timeEnd");
            completion = arrData.getJSONObject(i).getBoolean("completion");
            importance = arrData.getJSONObject(i).getBoolean("importance");

            resultMap = createMap(
                new ToDo(id, userId, name, date, startTime, endTime, completion, importance),
                resultMap);
        }

        return resultMap;
    }

    public static ToDo makeTask(JSONObject data) throws ParseException {
        JSONObject taskData = data.getJSONObject("data");

        final String id;
        final String userId;
        final String name;
        final String date;
        final String startTime;
        final String endTime;
        final boolean completion;
        final boolean importance;

        id = taskData.getString("id");
        userId = taskData.getString("userId");
        name = taskData.getString("todoName");
        date = taskData.getString("date");
        startTime = taskData.getString("timeStart");
        endTime = taskData.getString("timeEnd");
        completion = taskData.getBoolean("completion");
        importance = taskData.getBoolean("importance");

        return new ToDo(id, userId, name, date, startTime, endTime, completion, importance);
    }

    public static SortedMap<Date, PriorityQueue<ToDo>> createMap(
        ToDo task, SortedMap<Date, PriorityQueue<ToDo>> resultMap) {
        if (resultMap.containsKey(task.getDay())) {
            resultMap.get(task.getDay()).add(task);
        } else {
            PriorityQueue<ToDo> listTasks = new PriorityQueue<>();
            listTasks.add(task);
            resultMap.put(task.getDay(), listTasks);
        }
        return resultMap;
    }
}