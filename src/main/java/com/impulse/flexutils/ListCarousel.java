package com.impulse.flexutils;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.SortedMap;
import java.util.function.Supplier;


public class ListCarousel implements Supplier<FlexMessage> {
    private Carousel carousel;
    private SortedMap<Date, PriorityQueue<ToDo>> schedule;
    private List<Bubble> listBubbles = new ArrayList<>();


    @Override
    public FlexMessage get() {
        return new FlexMessage("List", this.carousel);
    }

    public FlexMessage get(SortedMap<Date,PriorityQueue<ToDo>> schedule) {
        this.schedule = schedule;
        if (this.schedule.isEmpty()) {
            this.carousel = Carousel.builder()
                .contents(getEmptyList())
                .build();
        } else if (allTaskCompleted()) {
            this.carousel = Carousel.builder()
                .contents(getAllCompletedList())
                .build();
        } else {
            this.carousel = Carousel.builder()
                .contents(getDayList())
                .build();
        }
        return get();
    }

    public List<Bubble> getDayList() {
        this.schedule.forEach((period, tasks) -> this.listBubbles
            .add(new DayBubble().create(period, tasks)));
        return this.listBubbles;
    }

    public List<Bubble> getEmptyList() {
        this.listBubbles.add(new EmptyBubble()
            .create("You Have No ToDo Yet", "Add one now!"));
        return this.listBubbles;
    }

    public List<Bubble> getAllCompletedList() {
        this.listBubbles.add(new EmptyBubble()
            .create("You Have Completed All of Your ToDo(s)", "Add another now!"));
        return this.listBubbles;
    }

    public boolean allTaskCompleted() {
        boolean allCompleted = true;
        loop:
        for (SortedMap.Entry<Date, PriorityQueue<ToDo>> toDoMap : this.schedule.entrySet()) {
            PriorityQueue<ToDo> tasks = toDoMap.getValue();
            for (ToDo task : tasks) {
                if (!task.getCompletion()) {
                    allCompleted = false;
                    break loop;
                }
            }
        }
        return allCompleted;
    }
}