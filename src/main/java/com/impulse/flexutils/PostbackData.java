package com.impulse.flexutils;

import org.json.JSONObject;

public class PostbackData {

    public JSONObject getData(ToDo task) {
        return new JSONObject("{"
            + "\"success\": true,"
            + "\"data\":"
            + "{\"id\":\"" + task.getId() + "\","
            + "\"userId\":\"" + task.getUserId() + "\","
            + "\"todoName\":\"" + task.getName() + "\","
            + "\"date\":\"" + task.getDate() + "\","
            + "\"timeStart\":\"" + task.getStartTime() + "\","
            + "\"timeEnd\":\"" + task.getEndTime() + "\","
            + "\"completion\":" + task.getCompletion() + ","
            + "\"importance\":" + task.getImportance() + "}"
            + "}"
        );
    }
}