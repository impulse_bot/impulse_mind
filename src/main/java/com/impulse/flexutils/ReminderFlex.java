package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.function.Supplier;


public class ReminderFlex implements Supplier<FlexMessage> {
    @Override
    public FlexMessage get() {
        final Box bodyBlock = createBodyBlock();
        final Box footerBlock = createFooterBlock();
        final Bubble bubble =
            Bubble.builder()
                .body(bodyBlock)
                .footer(footerBlock)
                .build();
        return new FlexMessage("Reminder", bubble);
    }

    private Box createBodyBlock() {
        final Text header =
            Text.builder()
                .text("ALERT")
                .size(FlexFontSize.XL)
                .align(FlexAlign.CENTER)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.BOLD)
                .color("#283592")
                .build();
        final Text reminder =
            Text.builder()
                .text("Don't forget to complete one of your tasks today :)")
                .margin(FlexMarginSize.XL)
                .size(FlexFontSize.Md)
                .align(FlexAlign.CENTER)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.REGULAR)
                .color("#000000")
                .wrap(true)
                .build();
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(header, reminder))
            .build();
    }

    private Box createFooterBlock() {
        return Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .contents(asList(
                Text.builder()
                    .text("Have a nice day!")
                    .size(FlexFontSize.XL)
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .weight(TextWeight.BOLD)
                    .color("#E01B84")
                    .build()
            ))
            .build();
    }
}