package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.SortedMap;
import java.util.function.Supplier;


public class ReviewFlex implements Supplier<FlexMessage> {
    private Bubble bubble;
    private Boolean allIncomplete = false;


    @Override
    public FlexMessage get() {
        return new FlexMessage("Review", this.bubble);
    }

    public FlexMessage get(SortedMap<Date, PriorityQueue<ToDo>> schedule) {
        final Box headerBlock;
        final Box bodyBlock;
        final Box footerBlock;

        if (schedule.isEmpty()) {
            this.bubble = new EmptyBubble()
                .create("You Have No ToDo Yet", "Add one now!");
        } else {
            bodyBlock = createBodyBlock(schedule);
            if (this.allIncomplete) {
                this.bubble = new EmptyBubble()
                    .create("You Have No Completed ToDo Yet", "Complete one now!");
            } else {
                headerBlock = createHeaderBlock();
                footerBlock = createFooterBlock();
                this.bubble = Bubble.builder()
                    .header(headerBlock)
                    .body(bodyBlock)
                    .footer(footerBlock)
                    .build();
            }
        }
        return get();
    }

    private Box createHeaderBlock() {
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(
                Text.builder()
                    .text("Your Completed Tasks")
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .weight(TextWeight.BOLD)
                    .color("#283592")
                    .build(),
                Separator.builder()
                    .margin(FlexMarginSize.NONE)
                    .color("#000000")
                    .build()
            ))
            .build();
    }

    private Box createBodyBlock(SortedMap<Date,PriorityQueue<ToDo>> schedule) {
        List<FlexComponent> bodyContents = new ArrayList<>();
        for (SortedMap.Entry<Date, PriorityQueue<ToDo>> toDoMap : schedule.entrySet()) {
            Box tasks = createCompletedTasks(toDoMap.getValue());
            if (bodyContents.size() < 10 && tasks != null) {
                bodyContents.add(tasks);
            }
        }
        if (bodyContents.isEmpty()) {
            this.allIncomplete = true;
        }
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.LG)
            .contents(bodyContents)
            .build();
    }

    private Box createCompletedTasks(PriorityQueue<ToDo> tasks) {
        List<FlexComponent> bodyContents = new ArrayList<>();
        tasks.forEach(task -> {
            if (task.getCompletion()) {
                bodyContents.add(makeCompletedButton(task));
            }
        });
        if (bodyContents.isEmpty()) {
            return null;
        } else {
            return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(bodyContents)
                .build();
        }
    }

    private Button makeCompletedButton(ToDo task) {
        return Button.builder()
            .action(new PostbackAction(task.getName(),
                "action=DETAILS&data=" + (new PostbackData()).getData(task)))
            .flex(0)
            .style(Button.ButtonStyle.SECONDARY)
            .gravity(FlexGravity.CENTER)
            .build();
    }

    private Box createFooterBlock() {
        return Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .contents(asList(
                Image.builder()
                    .url("https://drive.google.com/uc?export=view&id=17Vh1hZS78Zch335D7p4sdYgoDHe9BqXs")
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .size(Image.ImageSize.XS)
                    .aspectRatio(Image.ImageAspectRatio.R1TO1)
                    .aspectMode(Image.ImageAspectMode.Fit)
                    .build()
            ))
            .build();
    }
}