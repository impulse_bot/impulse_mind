package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.SortedMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ShareFlex implements Supplier<FlexMessage> {
    private Bubble bubble;
    private boolean allCompleted = false;


    @Override
    public FlexMessage get() {
        return new FlexMessage("Share", this.bubble);
    }

    public FlexMessage get(SortedMap<Date, PriorityQueue<ToDo>> schedule)
        throws UnsupportedEncodingException {
        final Box headerBlock;
        final Box bodyBlock;
        final Box footerBlock;

        if (schedule.isEmpty()) {
            this.bubble = new EmptyBubble()
                .create("You Have No ToDo Yet", "Add one now!");
        } else {
            bodyBlock = createBodyBlock(schedule);
            if (this.allCompleted) {
                this.bubble = new EmptyBubble()
                    .create("You Have Completed All of Your ToDo(s)", "Add another now!");
            } else {
                headerBlock = createHeaderBlock();
                footerBlock = createFooterBlock(convertToString(schedule));
                this.bubble = Bubble.builder()
                    .header(headerBlock)
                    .body(bodyBlock)
                    .footer(footerBlock)
                    .build();
            }
        }
        return get();
    }

    private Box createHeaderBlock() {
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(
                Text.builder()
                    .text("Your Copyable Schedule")
                    .size(FlexFontSize.LG)
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .weight(TextWeight.BOLD)
                    .color("#283592")
                    .build()
            ))
            .build();
    }

    private Box createBodyBlock(SortedMap<Date, PriorityQueue<ToDo>> schedule) {
        List<FlexComponent> bodyContents = new ArrayList<>();
        for (SortedMap.Entry<Date, PriorityQueue<ToDo>> toDoMap : schedule.entrySet()) {
            Box tasksByPeriod = createTasks(toDoMap.getKey(), toDoMap.getValue());
            if (tasksByPeriod != null) {
                bodyContents.add(tasksByPeriod);
            }
        }
        if (bodyContents.isEmpty()) {
            this.allCompleted = true;
        }
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.XL)
            .contents(bodyContents)
            .build();
    }

    private Box createTasks(Date period, PriorityQueue<ToDo> tasksList) {
        List<FlexComponent> tasks = new ArrayList<>();

        List<FlexComponent> checkTasks = new ArrayList<>();
        tasksList.stream().forEach(task -> {
            if (!task.getCompletion()) {
                checkTasks.add(createTask(task));
            }
        });

        if (checkTasks.isEmpty()) {
            return null;
        } else {
            tasks.add(
                Text.builder()
                    .text(new SimpleDateFormat("EEE, dd/MM/yyyy").format(period))
                    .size(FlexFontSize.SM)
                    .align(FlexAlign.START)
                    .gravity(FlexGravity.CENTER)
                    .weight(TextWeight.BOLD)
                    .color("#E01B84")
                    .build()
            );

            tasks.add(
                Separator.builder()
                    .margin(FlexMarginSize.NONE)
                    .color("#283592")
                    .build()
            );

            tasks.addAll(checkTasks);

            return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(tasks)
                .build();
        }
    }

    private Box createTask(ToDo task) {
        final Text name =
            Text.builder()
                .text(task.getName())
                .size(FlexFontSize.SM)
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.REGULAR)
                .color("#000000")
                .wrap(true)
                .build();

        final Text time =
            Text.builder()
                .text(task.getStartTime() + " - " + task.getEndTime())
                .size(FlexFontSize.SM)
                .align(FlexAlign.END)
                .gravity(FlexGravity.CENTER)
                .weight(TextWeight.REGULAR)
                .color("#000000")
                .build();

        return Box.builder()
            .layout(FlexLayout.BASELINE)
            .spacing(FlexMarginSize.XXL)
            .contents(asList(name, time))
            .build();
    }

    private String convertToString(SortedMap<Date, PriorityQueue<ToDo>> schedule) {
        final StringBuilder result = new StringBuilder();
        for (SortedMap.Entry<Date, PriorityQueue<ToDo>> entry : schedule.entrySet()) {
            result.append(new SimpleDateFormat("EEE, dd/MM/yyyy").format(entry.getKey()) + "\n");
            PriorityQueue<ToDo> tasks = entry.getValue();
            List<ToDo> tasksList = tasks.stream()
                .sorted(ToDo.ToDoComparator)
                .collect(Collectors.toList());
            for (ToDo task : tasksList) {
                result.append("- " + task.getName()
                    + " (" + task.getStartTime() + " - " + task.getEndTime() + ")\n");
            }
            result.append("\n");
        }
        return result.toString();
    }

    private Box createFooterBlock(String shareData) throws UnsupportedEncodingException {
        return Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .contents(asList(
                Button.builder()
                    .action(new URIAction("Share", "line://msg/text/"
                        + URLEncoder.encode(shareData, StandardCharsets.UTF_8.toString())))
                    .color("#283592")
                    .style(Button.ButtonStyle.PRIMARY)
                    .gravity(FlexGravity.CENTER)
                    .build()
            ))
            .build();
    }
}