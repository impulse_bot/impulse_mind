package com.impulse.flexutils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;


public class ToDo implements Comparable<ToDo> {
    private String id;
    private String userId;
    private String name;
    private String date;
    private String startTime;
    private String endTime;
    private boolean completion;
    private boolean importance;
    private Date comparator;
    private String period;
    private Date day;


    public ToDo(String id, String userId, String name, String date, String startTime,
                String endTime, boolean completion, boolean importance) throws ParseException {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.completion = completion;
        this.importance = importance;
        this.comparator = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date + " " + startTime);
        this.period = new SimpleDateFormat("EEE, dd/MM/yyyy").format(comparator);
        this.day = new SimpleDateFormat("EEE, dd/MM/yyyy").parse(this.period);
    }

    public String getId() {
        return this.id;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getName() {
        return this.name;
    }

    public String getDate() {
        return this.date;
    }

    public String getPeriod() {
        return this.period;
    }

    public String getStartTime() {
        return this.startTime;
    }

    public String getEndTime() {
        return this.endTime;
    }

    public Date getComparator() {
        return this.comparator;
    }

    public Date getDay() {
        return this.day;
    }

    public boolean getImportance() {
        return this.importance;
    }

    public boolean getCompletion() {
        return this.completion;
    }

    @Override
    public int compareTo(ToDo other) {
        if (this.getComparator().compareTo(other.getComparator()) < 0) {
            return ((!this.getImportance()) && other.getImportance()) ? 1 : -1;
        } else if (this.getComparator().compareTo(other.getComparator()) > 0) {
            return (this.getImportance() && (!other.getImportance())) ? -1 : 1;
        }
        return 0;
    }

    public static Comparator<ToDo> ToDoComparator = (task1, task2) -> task1.compareTo(task2);
}