package com.impulse.flexutils;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.function.Supplier;


public class WelcomeFlex implements Supplier<FlexMessage> {
    @Override
    public FlexMessage get() {
        final Box headerBlock = createHeaderBlock();
        final Image heroBlock = createHeroBlock();
        final Box bodyBlock = createBodyBlock();
        final Box footerBlock = createFooterBlock();
        final Bubble bubble =
            Bubble.builder()
                .header(headerBlock)
                .hero(heroBlock)
                .body(bodyBlock)
                .footer(footerBlock)
                .build();
        return new FlexMessage("Welcome", bubble);
    }

    private Box createHeaderBlock() {
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .contents(asList(
                Text.builder()
                    .text("IMPULSE")
                    .size(FlexFontSize.XXL)
                    .align(FlexAlign.CENTER)
                    .gravity(FlexGravity.CENTER)
                    .weight(TextWeight.BOLD)
                    .color("#283592")
                    .build()
            ))
            .build();
    }

    private Image createHeroBlock() {
        return Image.builder()
            .url("https://drive.google.com/uc?export=view&id=17Vh1hZS78Zch335D7p4sdYgoDHe9BqXs")
            .align(FlexAlign.CENTER)
            .gravity(FlexGravity.CENTER)
            .size(Image.ImageSize.FULL_WIDTH)
            .aspectRatio(Image.ImageAspectRatio.R4TO3)
            .aspectMode(Image.ImageAspectMode.Fit)
            .build();
    }

    private Box createBodyBlock() {
        final Text name =
            Text.builder()
                .text("Hello!")
                .align(FlexAlign.CENTER)
                .gravity(FlexGravity.CENTER)
                .color("#000000")
                .build();
        final Text info =
            Text.builder()
                .text("I will help you to stay organized")
                .align(FlexAlign.CENTER)
                .gravity(FlexGravity.CENTER)
                .color("#000000")
                .build();
        return Box.builder()
            .layout(FlexLayout.VERTICAL)
            .spacing(FlexMarginSize.XS)
            .contents(asList(name, info))
            .build();
    }

    private Box createFooterBlock() {
        return Box.builder()
            .layout(FlexLayout.HORIZONTAL)
            .contents(asList(
                Button.builder()
                    .action(new MessageAction("Want to know more?", "help"))
                    .style(Button.ButtonStyle.PRIMARY)
                    .color("#6D64E8")
                    .gravity(FlexGravity.CENTER)
                    .build()
            ))
            .build();
    }
}