package com.impulse.jsonparser;
 
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

public class JsonCreator {
    public static JSONObject parser(String[] listData,String idUser) 
        throws IllegalArgumentException, ParseException {
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm");
        Date initialDate = new SimpleDateFormat("yyyy-MM-dd").parse(listData[1]);  
        Date startTime = timeFormat.parse(listData[2]);
        Date endTime = timeFormat.parse(listData[3]);
        if (startTime.compareTo(endTime) > 0) {
            throw new IllegalArgumentException("Wrong date"); 
        }

        String dataDate = "";
        dataDate = dateFormat.format(initialDate); 
        String dataStartTime = "";
        dataStartTime = timeFormat.format(startTime);
    
        String dataName = listData[0];
        JSONObject dataDetails = new JSONObject();
        dataDetails.put("todoName", dataName);
        dataDetails.put("userId", idUser);
        dataDetails.put("date", dataDate);
        dataDetails.put("timeStart", dataStartTime);
        String dataEndTime = "";
        dataEndTime = timeFormat.format(endTime); 
        dataDetails.put("timeEnd", dataEndTime);
        dataDetails.put("importance", false);
        dataDetails.put("completion", false);

        return dataDetails;
    }
}