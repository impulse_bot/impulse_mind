package com.impulse.mind;

import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.List;

public class MessageBuilder {
    List<Message> messageList;

    public MessageBuilder() {
        messageList = new ArrayList<Message>();
    }

    public void addMessage(Message message) {
        messageList.add(message);
    }

    public List<Message> getMessageList() {
        return messageList;
    }
}