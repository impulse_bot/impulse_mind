package com.impulse.mind;

import com.impulse.flexutils.FlexChooser;
import com.impulse.jsonparser.JsonCreator;
import com.impulse.requestutils.RequestCaller;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@LineMessageHandler
public class MessageController {

    public static final String baseApiUrl = "https://impulse-api.herokuapp.com/";

    private LineMessagingClient lineMessagingClient;
    
    protected RequestCaller requestCaller; // THIS IS THE CURRENT STRATEGY

    protected RequestCaller deleteCaller;

    protected RequestCaller putCaller;
    
    protected RequestCaller postCaller;
    
    protected RequestCaller getCaller;

    @Autowired
    public MessageController(
            LineMessagingClient lineMessagingClient, 
            @Qualifier("getCaller") RequestCaller getCaller, 
            @Qualifier("postCaller") RequestCaller postCaller, 
            @Qualifier("putCaller") RequestCaller putCaller, 
            @Qualifier("deleteCaller") RequestCaller deleteCaller) {
        this.lineMessagingClient = lineMessagingClient;
        this.postCaller = postCaller;
        this.getCaller = getCaller;
        this.putCaller = putCaller;
        this.deleteCaller = deleteCaller;
    }

    @EventMapping
    public void handleFollowEvent(FollowEvent followEvent) {
        String replyToken = followEvent.getReplyToken();
        FlexMessage responseFollowFlexMessage = FlexChooser.choose("WELCOME");
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.addMessage(responseFollowFlexMessage);
        reply(replyToken, messageBuilder.getMessageList());
    }

    @EventMapping
    public void handleTextMessageEvent(
        MessageEvent<TextMessageContent> messageEvent
    ) throws IOException {
        String input = messageEvent.getMessage().getText();
        String[] keywords = input.split("\\r?\\n");
        String trigger = keywords[0].toUpperCase();
        JSONObject jsonObjResponse = null;
        MessageBuilder messageBuilder = new MessageBuilder();
        String replyToken = messageEvent.getReplyToken();
        String apiUrl = baseApiUrl;
        String userId = messageEvent.getSource().getUserId();
        try {
            switch (trigger) {
                case ("HELP"):
                    FlexMessage responseHelpFlexMessage = FlexChooser.choose("HELP");
                    messageBuilder.addMessage(responseHelpFlexMessage);
                    break;
                case ("ADD"):
                    setRequestCaller(postCaller);
                    apiUrl += "todos/";
                    String[] data = Arrays.copyOfRange(keywords, 1, keywords.length);
                    JSONObject jsonObject = JsonCreator.parser(data, userId);
                    String jsonString = jsonObject.toString();
                    jsonObjResponse = requestCaller.makeRequest(apiUrl, jsonString);
                    TextMessage responseAdd = null;
                    if ((boolean)jsonObjResponse.get("success")) {
                        responseAdd = createTextMessage("Todo added successfully!");
                    } else {
                        responseAdd = createTextMessage("Todo couldn't be added..");
                    }
                    messageBuilder.addMessage(responseAdd);
                    break;
                case ("LIST"):
                    setRequestCaller(getCaller);
                    apiUrl += "user_todos/" + userId + "/";
                    jsonObjResponse = requestCaller.makeRequest(apiUrl, "");
                    FlexMessage responseList = FlexChooser.choose("LIST", jsonObjResponse);
                    messageBuilder.addMessage(responseList);
                    break;
                case ("REVIEW"):
                    setRequestCaller(getCaller);
                    apiUrl += "user_todos/" + userId + "/";
                    jsonObjResponse = requestCaller.makeRequest(apiUrl, "");
                    FlexMessage responseReview = FlexChooser.choose("REVIEW", jsonObjResponse);
                    messageBuilder.addMessage(responseReview);
                    break;
                case ("SHARE"):
                    setRequestCaller(getCaller);
                    apiUrl += "user_todos/" + userId + "/";
                    jsonObjResponse = requestCaller.makeRequest(apiUrl, "");
                    FlexMessage responseShare = FlexChooser.choose("SHARE", jsonObjResponse);
                    messageBuilder.addMessage(responseShare);
                    break;
                default:
                    String responseString = "Sorry, we couldn't get it.. maybe get some help?";
                    TextMessage responseUnknownTextMessage = createTextMessage(responseString);
                    messageBuilder.addMessage(responseUnknownTextMessage);
                    FlexMessage responseUnknownHelp = FlexChooser.choose("HELP");
                    messageBuilder.addMessage(responseUnknownHelp);
            }
        } catch (Exception requestException) {
            String errorString = "Sorry, something wrong happened on our side :(";
            TextMessage responseErrorTextMessage = createTextMessage(errorString);
            messageBuilder.addMessage(responseErrorTextMessage);
        } finally {
            reply(replyToken, messageBuilder.getMessageList());
        }
    }

    @EventMapping
    public void handlePostbackEvent(PostbackEvent event) throws IOException {
        String replyToken = event.getReplyToken();
        String postbackData = event.getPostbackContent().getData();
        String[] postBackDataEntries = postbackData.split("&");
        String action = postBackDataEntries[0].split("=")[1];
        String todoDetails = postBackDataEntries[1].split("=")[1];
        MessageBuilder messageBuilder = new MessageBuilder();
        JSONObject todoJsonObject = new JSONObject(todoDetails);
        String todoId = todoJsonObject.getJSONObject("data").getString("id");
        JSONObject resultJsonObject;
        String apiUrl = baseApiUrl + "todos/" + todoId + "/";
        JSONObject todoData = (JSONObject)todoJsonObject.get("data");
        try {
            switch (action) {
                case ("DETAILS"):
                    setRequestCaller(getCaller);
                    resultJsonObject = requestCaller.makeRequest(apiUrl, todoJsonObject.toString());
                    FlexMessage detailsResponse = FlexChooser.choose("DETAILS", todoJsonObject);
                    messageBuilder.addMessage(detailsResponse);
                    break;
                case ("DELETE"):
                    setRequestCaller(deleteCaller);
                    resultJsonObject = requestCaller.makeRequest(apiUrl, todoJsonObject.toString());
                    TextMessage deleteResponse = createTextMessage("Todo deleted..");
                    messageBuilder.addMessage(deleteResponse);
                    break;
                case ("IMPORTANT"):
                    setRequestCaller(putCaller);
                    todoData.put("importance", true);
                    resultJsonObject = requestCaller.makeRequest(apiUrl, todoData.toString());
                    TextMessage importantResponse = createTextMessage("Todo set as IMPORTANT");
                    messageBuilder.addMessage(importantResponse);
                    break;
                case ("UNIMPORTANT"):
                    setRequestCaller(putCaller);;
                    todoData.put("importance", false);
                    resultJsonObject = requestCaller.makeRequest(apiUrl, todoData.toString());
                    TextMessage unimportantResponse = createTextMessage("Todo set as NORMAL");
                    messageBuilder.addMessage(unimportantResponse);
                    break;
                case ("COMPLETE"):
                    setRequestCaller(putCaller);
                    todoData.put("completion", true);
                    resultJsonObject = requestCaller.makeRequest(apiUrl, todoData.toString());
                    TextMessage completeResponse = createTextMessage("Todo completed!");
                    messageBuilder.addMessage(completeResponse);
                    break;
                case ("INCOMPLETE"):
                    setRequestCaller(putCaller);
                    todoData.put("completion", false);
                    resultJsonObject = requestCaller.makeRequest(apiUrl, todoData.toString());
                    TextMessage incompleteResponse = createTextMessage("Todo cancelled..");
                    messageBuilder.addMessage(incompleteResponse);
                    break;
                default:
                    throw new IOException();
            }
        } catch (Exception requestException) {
            String errorString = "Sorry, something wrong happened on our side :(";
            TextMessage responseErrorTextMessage = createTextMessage(errorString);
            messageBuilder.addMessage(responseErrorTextMessage);
        } finally {
            reply(replyToken, messageBuilder.getMessageList());
        }
    }

    public void setRequestCaller(RequestCaller caller) {
        requestCaller = caller;
    }

    private TextMessage createTextMessage(String content) {
        return new TextMessage(content);
    }

    protected void reply(String replyToken, List<Message> messages) {
        try {
            lineMessagingClient.replyMessage(
                new ReplyMessage(replyToken, messages)
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}