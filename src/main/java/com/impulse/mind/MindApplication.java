package com.impulse.mind;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = { 
    "com.impulse.mind", 
    "com.impulse.requestutils", 
    "com.impulse.flexutils", 
    "com.impulse.jsonparser"
})
public class MindApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(MindApplication.class, args);
    }
}
