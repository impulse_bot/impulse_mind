package com.impulse.requestutils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component("deleteCaller")
public class DeleteCaller implements RequestCaller {

    public JSONObject makeRequest(String urlString, String paramString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection httpCon = (HttpURLConnection)url.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestProperty("User-Agent", "ImpulseMind");
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestMethod("DELETE");
        InputStream inputStream = httpCon.getInputStream();
        Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
        String result = scanner.hasNext() ? scanner.next() : "";
        scanner.close();
        JSONObject json = new JSONObject(result);
        httpCon.disconnect();
        return json;
    }
}