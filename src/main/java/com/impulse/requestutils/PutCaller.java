package com.impulse.requestutils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component("putCaller")
public class PutCaller implements RequestCaller {

    public JSONObject makeRequest(String urlString, String paramString) throws IOException {
        byte[] postData = paramString.getBytes(StandardCharsets.UTF_8);
        URL url = new URL(urlString);
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestMethod("PUT");
        httpCon.setRequestProperty("User-Agent", "ImpulseMind");
        httpCon.setRequestProperty("Content-Type", "application/json");
        OutputStreamWriter out = new OutputStreamWriter(
            httpCon.getOutputStream());
        try (DataOutputStream wr = new DataOutputStream(httpCon.getOutputStream())) {
            wr.write(postData);
        }
        InputStream inputStream = httpCon.getInputStream();
        Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
        String result = scanner.hasNext() ? scanner.next() : "";
        out.close();
        scanner.close();
        JSONObject json = new JSONObject(result);
        httpCon.disconnect();
        return json;
    }
}