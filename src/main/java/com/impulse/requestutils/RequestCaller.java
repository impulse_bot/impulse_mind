package com.impulse.requestutils;

import java.io.IOException;
import org.json.JSONObject;

public interface RequestCaller {

    public JSONObject makeRequest(String urlString, String paramString) throws IOException;

}