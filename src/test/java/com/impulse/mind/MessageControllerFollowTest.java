package com.impulse.mind;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.impulse.flexutils.FlexChooser;
import com.impulse.requestutils.RequestCaller;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.response.BotApiResponse;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MessageControllerFollowTest {

    @Mock(name = "getCaller")
    private RequestCaller getCaller;

    @Mock(name = "postCaller")
    private RequestCaller postCaller;

    @Mock(name = "putCaller")
    private RequestCaller putCaller;

    @Mock(name = "deleteCaller")
    private RequestCaller deleteCaller;

    @Mock
    private LineMessagingClient lineMessagingClient;

    @InjectMocks
    private MessageController messageController;
        
    static {
        System.setProperty("line.bot.channelSecret", "SECRET");
        System.setProperty("line.bot.channelToken", "TOKEN");
    }

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
        messageController =  new MessageController(
            lineMessagingClient,
            getCaller,
            postCaller,
            putCaller,
            deleteCaller
        );
    }
    
    @Test
    public void contextLoads() {
    }

    @Test
    public void testContextLoads() {
        Assert.assertNotNull(messageController);
    }

    @Test
    public void testFollowEvent() throws Exception {
        FollowEvent followEvent = TestRequestResponseUtil.createFollowEvent();
        Message expectedResponse = FlexChooser.choose("WELCOME");
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(
                CompletableFuture.completedFuture(new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleFollowEvent(followEvent);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

}
