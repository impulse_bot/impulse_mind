package com.impulse.mind;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.impulse.flexutils.FlexChooser;
import com.impulse.requestutils.RequestCaller;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class MessageControllerPostBackTest {

    @Mock(name = "getCaller")
    private RequestCaller getCaller;

    @Mock(name = "postCaller")
    private RequestCaller postCaller;

    @Mock(name = "putCaller")
    private RequestCaller putCaller;

    @Mock(name = "deleteCaller")
    private RequestCaller deleteCaller;

    @Mock
    private LineMessagingClient lineMessagingClient;

    @InjectMocks
    private MessageController messageController;
        
    static {
        System.setProperty("line.bot.channelSecret", "SECRET");
        System.setProperty("line.bot.channelToken", "TOKEN");
    }

    public final String baseApiUrl = "http://localhost:8080/";

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
        messageController =  new MessageController(
            lineMessagingClient,
            getCaller,
            postCaller,
            putCaller,
            deleteCaller
        );
    }

    @Test
    public void testHandlePostBackDetails() throws Exception {
        String url = baseApiUrl + "todos/todoId/";
        JSONObject todoListJson = TestRequestResponseUtil.getSingleTodoJson();
        when(getCaller.makeRequest(url, "")).thenReturn(todoListJson);
        Message expectedResponse = FlexChooser.choose("DETAILS", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        PostbackEvent send = TestRequestResponseUtil.createPostBackEvent("DETAILS",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    
    }

    @Test
    public void testHandlePostBackDetailsUnimportantAndDone() throws Exception {
        String url = baseApiUrl + "todos/todoId/";
        JSONObject todoListJson = TestRequestResponseUtil.getSingleNotImportantTodoJson();
        when(getCaller.makeRequest(url, "")).thenReturn(todoListJson);
        Message expectedResponse = FlexChooser.choose("DETAILS", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
        .thenReturn(CompletableFuture.completedFuture(
            new BotApiResponse("ok", Collections.emptyList())
        ));
        PostbackEvent send = TestRequestResponseUtil.createPostBackEvent("DETAILS",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    
    }

    @Test
    public void testHandlePostBackDelete() throws Exception {
        String url = baseApiUrl + "todos/todoId";
        JSONObject todoListJson = TestRequestResponseUtil.getSingleTodoJson();
        when(deleteCaller.makeRequest(url, "")).thenReturn(todoListJson);
        Message expectedMessage = new TextMessage("Todo deleted..");
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(deleteCaller.makeRequest(url, "")).thenThrow(new IOException());
        PostbackEvent send = TestRequestResponseUtil.createPostBackEvent("DELETE",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandlePostBackImportant() throws Exception {
        String url = baseApiUrl + "todos/todoId/";
        JSONObject todoListJson = TestRequestResponseUtil.getSingleTodoJson();
        when(putCaller.makeRequest(url, "")).thenReturn(todoListJson);
        Message expectedMessage = new TextMessage("Todo set as IMPORTANT");
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(putCaller.makeRequest(url, "")).thenThrow(new IOException());
        PostbackEvent send = TestRequestResponseUtil.createPostBackEvent("IMPORTANT",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));

    }

    @Test
    public void testHandlePostBackUnimportant() throws Exception {
        String url = baseApiUrl + "todos/todoId/";
        JSONObject todoListJson = TestRequestResponseUtil.getSingleTodoJson();
        when(putCaller.makeRequest(url, "")).thenReturn(todoListJson);
        Message expectedMessage = new TextMessage("Todo set as NORMAL");
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(putCaller.makeRequest(url, "")).thenThrow(new IOException());
        PostbackEvent send = 
            TestRequestResponseUtil.createPostBackEvent("UNIMPORTANT",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));

    }

    @Test
    public void testHandlePostBackComplete() throws Exception {
        String url = baseApiUrl + "todos/todoId/";
        JSONObject todoListJson = TestRequestResponseUtil.getSingleTodoJson();
        when(putCaller.makeRequest(url, "")).thenReturn(todoListJson);
        Message expectedMessage = new TextMessage("Todo completed!");
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(putCaller.makeRequest(url, "")).thenThrow(new IOException());
        PostbackEvent send = TestRequestResponseUtil.createPostBackEvent("COMPLETE",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandlePostBackIncomplete() throws Exception {
        String url = baseApiUrl + "todos/todoId/";
        Message expectedMessage = new TextMessage("Todo cancelled..");
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        JSONObject todoListJson = TestRequestResponseUtil.getSingleTodoJson();
        when(putCaller.makeRequest(url, "")).thenReturn(todoListJson);
        when(putCaller.makeRequest(url, "")).thenThrow(new IOException());
        PostbackEvent send = TestRequestResponseUtil.createPostBackEvent("INCOMPLETE",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandlePostBackEventError() throws Exception {
        Message expectedResponseText = 
            new TextMessage("Sorry, something wrong happened on our side :(");
        List<Message> expectedMessage = new ArrayList<Message>();
        expectedMessage.add(expectedResponseText);
        JSONObject todoListJson = TestRequestResponseUtil.getSingleTodoJson();
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(getCaller.makeRequest(anyString(), anyString())).thenThrow(new IOException());
        PostbackEvent send = TestRequestResponseUtil.createPostBackEvent("DETAIL",todoListJson);
        messageController.handlePostbackEvent(send);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }
}