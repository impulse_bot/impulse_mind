package com.impulse.mind;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.impulse.flexutils.FlexChooser;
import com.impulse.requestutils.RequestCaller;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MessageControllerTextTest {

    @Mock(name = "getCaller")
    private RequestCaller getCaller;

    @Mock(name = "postCaller")
    private RequestCaller postCaller;

    @Mock(name = "putCaller")
    private RequestCaller putCaller;

    @Mock(name = "deleteCaller")
    private RequestCaller deleteCaller;

    @Mock
    private LineMessagingClient lineMessagingClient;

    @InjectMocks
    private MessageController messageController;

    private FlexChooser flexCreator = new FlexChooser();
        
    static {
        System.setProperty("line.bot.channelSecret", "SECRET");
        System.setProperty("line.bot.channelToken", "TOKEN");
    }

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
        messageController =  new MessageController(
            lineMessagingClient,
            getCaller,
            postCaller,
            putCaller,
            deleteCaller
        );
    }
    
    @Test
    public void contextLoads() {
    }

    @Test
    public void testContextLoads() {
        Assert.assertNotNull(messageController);
    }

    @Test
    public void testHandleTextEventHelp() throws Exception {
        String trigger = "HELP";
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        Message expectedResponse = flexCreator.choose("HELP");
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventUnknown() throws Exception {
        String trigger = "UNKNOWN";
        Message expectedResponseText = 
            new TextMessage("Sorry, we couldn't get it.. maybe get some help?");
        Message expectedResponseFlex = flexCreator.choose("HELP");
        List<Message> expectedMessage = new ArrayList<Message>();
        expectedMessage.add(expectedResponseText);
        expectedMessage.add(expectedResponseFlex);
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
      
    }

    @Test
    public void testHandleTextEventAddSuccess() throws Exception {
        String trigger = "ADD\ntodoName1\n2019-04-23\n09:00\n10:00";
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        JSONObject todoSingleJson = TestRequestResponseUtil.getSingleTodoJson();
        when(postCaller.makeRequest(anyString(), anyString())).thenReturn(todoSingleJson);
        Message expectedResponse = new TextMessage("Todo added successfully!");
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventAddFail() throws Exception {
        String trigger = "ADD\ntodoName1\n2019-04-23\n09:00\n10:00";
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        JSONObject todoSingleJson = TestRequestResponseUtil.getErrorResponse();
        when(postCaller.makeRequest(anyString(), anyString())).thenReturn(todoSingleJson);
        Message expectedResponse = new TextMessage("Todo couldn't be added..");
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventList() throws Exception {
        String trigger = "LIST";
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        JSONObject todoListJson = TestRequestResponseUtil.getListTodoJson();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponse = this.flexCreator.choose("LIST", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventReview() throws Exception {
        String trigger = "REVIEW";
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        JSONObject todoListJson = TestRequestResponseUtil.getListTodoCompletionJson();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponse = this.flexCreator.choose("REVIEW", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventReviewNotComplete() throws Exception {
        String trigger = "REVIEW";
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        JSONObject todoListJson = TestRequestResponseUtil.getListTodoInCompletionJson();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponse = this.flexCreator.choose("REVIEW", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    public void testHandleTextEventListCompare() throws Exception {
        String trigger = "LIST";
        MessageEvent<TextMessageContent> message = TestRequestResponseUtil.createTextMessage(trigger);
        // String url = API_URL + "user_todos/userId/";
        JSONObject todoListJson = TestRequestResponseUtil.getListTodoCompareJson();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponse = this.flexCreator.choose("LIST", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
        .thenReturn(CompletableFuture.completedFuture(
            new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventListCompletion() throws Exception {
        String trigger = "LIST";
        MessageEvent<TextMessageContent> message = TestRequestResponseUtil.createTextMessage(trigger);
        // String url = API_URL + "user_todos/userId/";
        JSONObject todoListJson = TestRequestResponseUtil.getListTodoCompletionJson();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponse = this.flexCreator.choose("LIST", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
        .thenReturn(CompletableFuture.completedFuture(
            new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventShare() throws Exception {
        String trigger = "SHARE";
        JSONObject todoListJson = TestRequestResponseUtil.getListTodoJson();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponseFlex = flexCreator.choose("SHARE", todoListJson);
        List<Message> expectedMessage = new ArrayList<Message>();
        expectedMessage.add(expectedResponseFlex);
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventShareEmpty() throws Exception {
        String trigger = "SHARE";
        JSONObject todoListJson = TestRequestResponseUtil.getListTodoEmptyJson();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponseFlex = flexCreator.choose("SHARE", todoListJson);
        List<Message> expectedMessage = new ArrayList<Message>();
        expectedMessage.add(expectedResponseFlex);
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventError() throws Exception {
        String trigger = "LIST";
        Message expectedResponseText = 
            new TextMessage("Sorry, something wrong happened on our side :(");
        List<Message> expectedMessage = new ArrayList<Message>();
        expectedMessage.add(expectedResponseText);
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        when(getCaller.makeRequest(anyString(), anyString())).thenThrow(new IOException());
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventAddDateFailed() throws Exception {
        String trigger = "ADD\ntodoName1\n2019-04-23\n10:00\n09:00";
        MessageEvent<TextMessageContent> message = 
            TestRequestResponseUtil.createTextMessage(trigger);
        Message expectedResponseText = 
            new TextMessage("Sorry, something wrong happened on our side :(");
        List<Message> expectedMessage = new ArrayList<Message>();
        expectedMessage.add(expectedResponseText);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventEmptyList() throws Exception {
        String trigger = "LIST";
        MessageEvent<TextMessageContent> message =
            TestRequestResponseUtil.createTextMessage(trigger);
        JSONObject todoListJson = TestRequestResponseUtil.getEmptyJsonList();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponse = this.flexCreator.choose("LIST", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }

    @Test
    public void testHandleTextEventEmptyReview() throws Exception {
        String trigger = "REVIEW";
        MessageEvent<TextMessageContent> message =
            TestRequestResponseUtil.createTextMessage(trigger);
        JSONObject todoListJson = TestRequestResponseUtil.getEmptyJsonList();
        when(getCaller.makeRequest(anyString(), anyString())).thenReturn(todoListJson);
        Message expectedResponse = this.flexCreator.choose("REVIEW", todoListJson);
        List<Message> expectedMessage = Collections.singletonList(expectedResponse);
        when(lineMessagingClient.replyMessage(
            new ReplyMessage("replyToken", expectedMessage)))
            .thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        messageController.handleTextMessageEvent(message);
        verify(lineMessagingClient, atLeastOnce())
            .replyMessage(new ReplyMessage("replyToken", expectedMessage));
    }
    
}
