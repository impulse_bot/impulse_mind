package com.impulse.mind;

import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.postback.PostbackContent;
import com.linecorp.bot.model.event.source.UserSource;
import java.time.Instant;
import lombok.experimental.UtilityClass;
import org.json.JSONObject;

@UtilityClass
public class TestRequestResponseUtil {
    public static MessageEvent<TextMessageContent> createTextMessage(final String text) {
        return new MessageEvent<>(
            "replyToken", 
            new UserSource("userId"),
            new TextMessageContent("id", text),
            Instant.now()
        );
    }
        
    public static PostbackEvent createPostBackEvent(String action, JSONObject todoDetails) {
        return new PostbackEvent(
            "replyToken", 
            new UserSource("userId"),
            new PostbackContent(
                "action=" + action + "&data=" + todoDetails.toString(), 
                null
            ), 
            Instant.now()
        );
    }

    public static FollowEvent createFollowEvent() {
        return new FollowEvent(
            "replyToken", 
            new UserSource("userId"), 
            Instant.now()
        );
    }

    public static JSONObject getListTodoJson() {
        return new JSONObject(
            "{"
                + "\"success\": true,"
                + "\"data\":["
                    + "{" 
                        + "\"id\":\"todoId1\","
                        + "\"userId\":\"userId\","
                        + "\"todoName\":\"todoName1\","
                        + "\"date\":\"2019-04-23\","
                        + "\"timeStart\":\"21:00\","
                        + "\"timeEnd\":\"22:00\","
                        + "\"completion\":false,"
                        + "\"importance\":true" 
                    + "},"
                    + "{"  
                        + "\"id\":\"todoId2\","
                        + "\"userId\":\"userId\","
                        + "\"todoName\":\"todoName2\","
                        + "\"date\":\"2019-04-23\","
                        + "\"timeStart\":\"23:00\","
                        + "\"timeEnd\":\"23:50\","
                        + "\"completion\":false,"
                        + "\"importance\":false"
                    + "}"
                + "]"
            + "}"
        );
    }

    public static JSONObject getListTodoCompareJson() {
        return new JSONObject(
            "{" +
                "\"success\": true," +
                "\"data\":[" +
                    "{" + 
                        "\"id\":\"todoId1\"," +
                        "\"userId\":\"userId\"," +
                        "\"todoName\":\"todoName1\"," +
                        "\"date\":\"2019-04-23\"," +
                        "\"timeStart\":\"21:00\"," +
                        "\"timeEnd\":\"22:00\"," +
                        "\"completion\":true," +
                        "\"importance\":true" + 
                    "}," +
                    "{" + 
                        "\"id\":\"todoId2\"," +
                        "\"userId\":\"userId\"," +
                        "\"todoName\":\"todoName2\"," +
                        "\"date\":\"2019-04-23\"," +
                        "\"timeStart\":\"23:00\"," +
                        "\"timeEnd\":\"23:50\"," +
                        "\"completion\":true," +
                        "\"importance\":true"+
                    "}" +
                "]" +
            "}"
        );
    }

    public static JSONObject getListTodoCompletionJson() {
        return new JSONObject(
            "{" +
                "\"success\": true," +
                "\"data\":[" +
                    "{" + 
                        "\"id\":\"todoId1\"," +
                        "\"userId\":\"userId\"," +
                        "\"todoName\":\"todoName1\"," +
                        "\"date\":\"2019-04-23\"," +
                        "\"timeStart\":\"21:00\"," +
                        "\"timeEnd\":\"22:00\"," +
                        "\"completion\":true," +
                        "\"importance\":false" + 
                    "}," +
                "]" +
            "}"
        );
    }

    public static JSONObject getListTodoInCompletionJson() {
        return new JSONObject(
            "{" +
                "\"success\": true," +
                "\"data\":[" +
                    "{" + 
                        "\"id\":\"todoId1\"," +
                        "\"userId\":\"userId\"," +
                        "\"todoName\":\"todoName1\"," +
                        "\"date\":\"2019-04-23\"," +
                        "\"timeStart\":\"21:00\"," +
                        "\"timeEnd\":\"22:00\"," +
                        "\"completion\":false," +
                        "\"importance\":false" + 
                    "}," +
                    "{" + 
                        "\"id\":\"todoId1\"," +
                        "\"userId\":\"userId\"," +
                        "\"todoName\":\"todoName1\"," +
                        "\"date\":\"2019-04-23\"," +
                        "\"timeStart\":\"21:00\"," +
                        "\"timeEnd\":\"22:00\"," +
                        "\"completion\":false," +
                        "\"importance\":false" + 
                    "}," +
                    "{" + 
                        "\"id\":\"todoId1\"," +
                        "\"userId\":\"userId\"," +
                        "\"todoName\":\"todoName1\"," +
                        "\"date\":\"2019-04-23\"," +
                        "\"timeStart\":\"21:00\"," +
                        "\"timeEnd\":\"22:00\"," +
                        "\"completion\":true," +
                        "\"importance\":false" + 
                    "}," +
                "]" +
            "}"
        );
    }

    public static JSONObject getSingleTodoJson() {
        return new JSONObject(
            "{"
            + "\"success\": true,"
            + "\"data\":"
                + "{"
                    + "\"id\":\"todoId1\","
                    + "\"userId\":\"userId\","
                    + "\"todoName\":\"todoName1\","
                    + "\"date\":\"2019-04-23\","
                    + "\"timeStart\":\"21:00\","
                    + "\"timeEnd\":\"22:00\","
                    + "\"completion\":false,"
                    + "\"importance\":true"
                + "}"
            + "}"
        );
    }

    public static JSONObject getSingleNotImportantTodoJson() {
        return new JSONObject(
            "{" +
            "\"success\": true," +
            "\"data\":" +
                "{" + 
                    "\"id\":\"todoId1\"," +
                    "\"userId\":\"userId\"," +
                    "\"todoName\":\"todoName1\"," +
                    "\"date\":\"2019-04-23\"," +
                    "\"timeStart\":\"21:00\"," +
                    "\"timeEnd\":\"22:00\"," +
                    "\"completion\":true," +
                    "\"importance\":false" + 
                "}" +
            "}"
        );
    }

    public static JSONObject getSingleTodoErrorJson() {
        return new JSONObject(
            "{"
            + "\"success\": true,"
            + "\"data\":"
                + "{"
                    + "\"id\":\"todoId1\","
                    + "\"userId\":\"userId\","
                    + "\"todoName\":\"todoName1\","
                    + "\"date\":\"2019-04-23\","
                    + "\"timeStart\":\"22:00\","
                    + "\"timeEnd\":\"21:00\","
                    + "\"completion\":false,"
                    + "\"importance\":true"
                + "}"
            + "}"
        );
    }
    
    public static JSONObject getErrorResponse() {
        return new JSONObject(
            "{"
                + "\"success\": false,"
                + "\"data\": [ERROR MSG]"
            + "}" 
        );
    }

    public static JSONObject getEmptyJsonList() {
        return new JSONObject(
            "{"
                + "\"success\": true,"
                + "\"data\": []"
            + "}" 
        );
    }

    public static JSONObject getListTodoEmptyJson() {
        return new JSONObject(
            "{" +
                "\"success\": true," +
                "\"data\":[" +
                "]" +
            "}"
        );
    }
}