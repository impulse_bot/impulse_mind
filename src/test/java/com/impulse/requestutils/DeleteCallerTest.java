package com.impulse.requestutils;

import static org.junit.Assert.assertEquals;

import com.impulse.requestutils.RequestCaller;
import java.io.ByteArrayInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DeleteCaller.class })
public class DeleteCallerTest {

    @Test
    public void deleteCallerRequestTest() throws Exception {
        RequestCaller deleteCallerMock = new DeleteCaller();
        URL url = PowerMockito.mock(URL.class);
        PowerMockito.whenNew(URL.class).withArguments(Mockito.anyString()).thenReturn(url);
        HttpURLConnection httpCon = PowerMockito.mock(HttpURLConnection.class);
        PowerMockito.when(url.openConnection()).thenReturn(httpCon);
        ByteArrayInputStream contents = 
            new ByteArrayInputStream("{\"content\":\"jsonConten\"}".getBytes());
        PowerMockito.when(httpCon.getInputStream()).thenReturn(contents);
        JSONObject jsonResponse = deleteCallerMock.makeRequest("urlString", "paramString");
        assertEquals("{\"content\":\"jsonConten\"}", jsonResponse.toString());
    }
}