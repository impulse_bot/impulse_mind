package com.impulse.requestutils;

import static org.junit.Assert.assertEquals;

import com.impulse.requestutils.RequestCaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PutCaller.class })
public class PutCallerTest {
    @Test
    public void putCallerRequestTest() throws Exception {
        RequestCaller putCallerMock = new PutCaller();
        URL url = PowerMockito.mock(URL.class);
        PowerMockito.whenNew(URL.class).withArguments(Mockito.anyString()).thenReturn(url);
        HttpURLConnection httpCon = PowerMockito.mock(HttpURLConnection.class);
        PowerMockito.when(url.openConnection()).thenReturn(httpCon);
        ByteArrayOutputStream putBody = 
            new ByteArrayOutputStream();
        PowerMockito.when(httpCon.getOutputStream()).thenReturn(putBody);
        ByteArrayInputStream contents = 
            new ByteArrayInputStream("{\"content\":\"jsonConten\"}".getBytes());
        PowerMockito.when(httpCon.getInputStream()).thenReturn(contents);
        JSONObject jsonResponse = putCallerMock.makeRequest("urlString", "paramString");
        assertEquals("{\"content\":\"jsonConten\"}", jsonResponse.toString());
    }
}